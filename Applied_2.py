import tensorflow as tf

x = tf.constant([[2,3,5],[3,4,6],[4,5,4]], dtype=tf.float64)
y = tf.constant([[12,13,2],[5,6,5],[4,7,3]], dtype=tf.float64)

cov_xx = 1 / (tf.shape(x)[0] - 1) * tf.reduce_sum((x - tf.reduce_mean(x))**2)
cov_yy = 1 / (tf.shape(x)[0] - 1) * tf.reduce_sum((y - tf.reduce_mean(y))**2)
cov_xy = 1 / (tf.shape(x)[0] - 1) * tf.reduce_sum((x - tf.reduce_mean(x)) * (y - tf.reduce_mean(y)))

with tf.Session() as sess:
    sess.run([cov_xx, cov_yy, cov_xy])
    print(cov_xx.eval(), cov_yy.eval(), cov_xy.eval())
    cov = tf.constant([[cov_xx.eval(), cov_xy.eval()], [cov_xy.eval(),
        cov_yy.eval()]])
    print(cov.eval())